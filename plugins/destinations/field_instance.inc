<?php

/**
 * @file
 * Support for field instance destinations.
 */

/**
 * Destination class implementing migration into {field_config_instance}.
 */
class MigrateDestinationFieldInstance extends MigrateDestination {
  static public function getKeySchema() {
    return array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'The primary identifier for a field instance.',
      ),
    );
  }

  public function __construct() {
    parent::__construct();
  }

  public function __toString() {
    $output = t('Field instance');
    return $output;
  }

  /**
   * Returns a list of fields available to be mapped for fields.
   *
   * @param Migration $migration
   *  Optionally, the migration containing this destination.
   * @return array
   *  Keys: machine names of the fields (to be passed to addFieldMapping)
   *  Values: Human-friendly descriptions of the fields.
   */
  public function fields($migration = NULL) {
    $fields = array(
      'field_name' => t("The name of the field. Each field name is unique within Field API. Non-deleted field names are unique. Maximum length is 32 characters."),
      'entity_type' => t('The entity type on which this field instance exists. Maximum length 32 characters.'),
      'bundle' => t('The bundle on which this field exists. Maximum length 128 characters.'),
      'label' => t('The field label'),
      'description' => t('Field description'),
      'required' => t('Indicates that field values are mandatory.'),
      'default_value_function' => t('If specified, this is called to determine the default value for form widgets.'),
      'settings' => t("A array of key/value pairs of field-type-specific settings. Each field type module defines and documents its own field settings. Each omitted setting is given the default value defined in its module's hook_field_info()."),
      'widget_type' => t('Omit to use the default specified in hook_field_info().'),
      'widget_settings' => t("An array of key/value pairs of widget-specific settings. Each omitted setting is given the default value defined in its module's hook_field_widget_info()."),
      'display' => t("An array keyed by view mode names, and valued by display settings for that view mode. Settings for the 'default' view mode will be added if not present, and each view mode in the definition will be completed with the following default values: label: 'above' type: the default formatter specified in hook_field_info(), settings: each omitted setting is given the default value specified in hook_field_formatter_info(). View modes not present in the definition are left empty, and the field will not be displayed in this mode."),
    );
    $fields += migrate_handler_invoke_all('field_instance', 'fields', $migration);
    return $fields;
  }

  /**
   * Import a single row.
   *
   * @param stdClass $field_instance
   *  Field instance object to build. Pre-filled with any fields mapped in the
   *  Migration.
   * @param $row
   *  Raw source data object - passed through to prepare/complete handlers.
   *
   * @return array|bool
   *  Array of key fields of the object that was saved if
   *  successful. FALSE on failure.
   */
  public function import(stdClass $field_instance, stdClass $row) {
    // Invoke migration prepare handlers
    $this->prepare($field_instance, $row);

    // Fields are handled as arrays, so clone the object to an array.
    $field_instance = clone $field_instance;
    $field_instance = (array) $field_instance;

    if (isset($field_instance['widget_type'])) {
      $field_instance['widget']['type'] = $field_instance['widget_type'];
    }
    if (isset($field_instance['widget_settings'])) {
      $field_instance['widget']['settings'] = $field_instance['widget_settings'];
    }

    $existing_instance = FALSE;
    if (!empty($field_instance['id'])) {
      drupal_static_reset('_field_info_field_cache');
      // We need to use a low-level function to load by ID.
      if ($read_instances = field_read_instances(array('id' => $field_instance['id']))) {
        $existing_instance = reset($read_instances);
        // The field instance update fails if certain fields have been changed.
        foreach (array('entity_type', 'field_name', 'bundle') as $immutable) {
          if (isset($field_instance[$immutable]) && ($field_instance[$immutable] !== $existing_instance[$immutable])) {
            return FALSE;
          }
        }
        // Load the field instance properly.
        drupal_static_reset('_field_info_field_cache');
        $existing_instance = field_info_instance($existing_instance['entity_type'], $existing_instance['field_name'], $existing_instance['bundle']);
        $field_instance += $existing_instance;
      }
    }

    $return = FALSE;
    if ($existing_instance) {
      migrate_instrument_start('field_update_instance');
      try {
        field_update_instance($field_instance);
        $this->numUpdated++;
        $return = TRUE;
      } catch (FieldException $e) {
        Migration::displayMessage(t('Failed to update existing field instance: !message',
          array('!message' => $e->getMessage())
        ));
      }
      migrate_instrument_stop('field_update_instance');
    }
    else {
      migrate_instrument_start('field_create_instance');
      try {
        $field_instance = field_create_instance($field_instance);
        $this->numCreated++;
        $return = TRUE;
      } catch (FieldException $e) {
        Migration::displayMessage(t('Failed to create field instance: !message',
          array('!message' => $e->getMessage())
        ));
      }
      migrate_instrument_stop('field_create_instance');
    }

    // Invoke migration complete handlers.
    $field_instance = (object) field_info_instance($field_instance['entity_type'], $field_instance['field_name'], $field_instance['bundle']);
    if ($return) {
      $return = array('id' => $field_instance->id);
    }
    $this->complete($field_instance, $row);

    return $return;
  }

  public function prepare($field_instance, stdClass $row) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    $field_instance->migrate = array(
      'machineName' => $migration->getMachineName(),
    );

    // Call any general handlers.
    migrate_handler_invoke_all('field_instance', 'prepare', $field_instance, $row);
    // Then call any prepare handler for this specific Migration.
    if (method_exists($migration, 'prepare')) {
      $migration->prepare($field_instance, $row);
    }
  }

  public function complete($field_instance, stdClass $row) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    $field_instance->migrate = array(
      'machineName' => $migration->getMachineName(),
    );
    // Call any general handlers.
    migrate_handler_invoke_all('field_instance', 'complete', $field_instance, $row);
    // Then call any complete handler for this specific Migration.
    if (method_exists($migration, 'complete')) {
      $migration->complete($field_instance, $row);
    }
  }

  /**
   * Delete a single field instance.
   *
   * @param $field_instances
   *  Array of field instances representing the key (in this case, just id).
   */
  public function rollback(array $field_instances) {
    $field_instance_id = reset($field_instances);

    migrate_instrument_start('field_delete_instance');
    $this->prepareRollback($field_instance_id);
    if ($field_instance = field_read_instances(array('id' => $field_instance_id))) {
      $field_instance = reset($field_instance);
      $field_instance = field_read_instance($field_instance['entity_type'], $field_instance['field_name'], $field_instance['bundle']);
      field_delete_instance($field_instance, FALSE);
    }
    $this->completeRollback($field_instance_id);
    migrate_instrument_stop('field_delete_instance');
  }

  /**
   * Give handlers a shot at cleaning up before a field instance is rolled back.
   *
   * @param $field_instance_id
   *  ID of the field instance about to be deleted.
   */
  public function prepareRollback($field_instance_id) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    // Call any general handlers.
    migrate_handler_invoke_all('field_instance', 'prepareRollback', $field_instance_id);
    // Then call any complete handler for this specific Migration.
    if (method_exists($migration, 'prepareRollback')) {
      $migration->prepareRollback($field_instance_id);
    }
  }

  /**
   * Give handlers a shot at cleaning up after a field instance is rolled back.
   *
   * @param $field_instance_id
   *  ID of the field instance which has been deleted.
   */
  public function completeRollback($field_instance_id) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    // Call any general handlers.
    migrate_handler_invoke_all('field_instance', 'completeRollback', $field_instance_id);
    // Then call any complete handler for this specific Migration.
    if (method_exists($migration, 'completeRollback')) {
      $migration->completeRollback($field_instance_id);
    }
  }
}
