<?php
/**
 * @file
 * Support for field_validation module rules on a field instance.
 */

class MigrateFieldValidationRuleFieldInstanceHandler extends MigrateDestinationHandler {

  /**
   * Constructor.
   */
  public function __construct() {
    $this->registerTypes(array('field_instance'));
  }

  /**
   * Implementation of MigrateDestinationHandler::fields().
   */
  public function fields($migration = NULL) {
    if (module_exists('field_validation')) {
      $rule_spec = array();
      $rule_spec[] = t('name: The machine name of the rule. This name must be unique across the entity_type/bundle.');
      $rule_spec[] = t('rulename: The human-readable name of the rule');
      $rule_spec[] = t('col: The field column to apply the rule to.');
      $rule_spec[] = t('validator: The field_validation rule machine name.');
      $rule_spec[] = t('settings: An array of settings expected by the validator.');
      $rule_spec[] = t('error_message: The error message to display when the rule causes validation to fail.');
      return array('validation_rules' => t('An array of field_validation module rules to apply to the instance. Each rule is itself an array with the following keys: !rule_spec', array('!rule_spec' => theme('item_list', array('items' => $rule_spec)))));
    }
    return array();
  }

  /**
   * Implementation of MigrateDestinationHandler::complete().
   */
  public function complete($instance, $row) {
    if (!module_exists('field_validation')) {
      return;
    }
    // If the instance failed due to missing key data, we cannot proceed.
    if (empty($instance->field_name) || empty($instance->entity_type) || empty($instance->bundle)) {
      return;
    }
    if (!empty($instance->validation_rules)) {
      foreach($instance->validation_rules as $rule) {
        // If pre-existing, delete ready for re-import.
        if (!empty($rule['name']) && !empty($rule['col'])) {
          db_delete('field_validation_rule')
            ->condition('name', $rule['name'])
            ->condition('field_name', $instance->field_name)
            ->condition('col', $rule['col'])
            ->condition('entity_type', $instance->entity_type)
            ->condition('bundle', $instance->bundle)
            ->execute();
        }
        if (!empty($instance->id) && $this->validateRule($instance, $rule)) {
          $rule += array(
            'settings' => array(),
            'field_name' => $instance->field_name,
            'entity_type' => $instance->entity_type,
            'bundle' => $instance->bundle,
          );
          if (FALSE === drupal_write_record('field_validation_rule', $rule)) {
            Migration::displayMessage(t('Failed to import field validation rule: Unexpected error inserting new database record.'));
          }
          else {
            ctools_export_load_object_reset('field_validation_rule');
          }
        }
      }
    }
  }

  /**
   * Helper to validate that a validation rule is well-formed.
   *
   * @param stdClass $instance
   *   The field instance over which the rule operates.
   * @param array $rule
   *   Array of rule properties. The name is altered by reference to make it
   *   unique if necessary.
   *
   * @return bool
   *   The validity of the rule array.
   */
  protected function validateRule($instance, array &$rule) {
    // Check all required properties are present.
    $required = array(
      'name' => 1,
      'rulename' => 1,
      'col' => 1,
      'validator' => 1,
      'error_message' => 1,
    );
    if ($missing = array_diff_key($required, $rule)) {
      Migration::displayMessage(t('Failed to import field validation rule, the following rule properties are missing: @missing',
        array('@missing' => implode(', ', array_keys($missing)))
      ));
      return FALSE;
    }
    // Check the rule name is not too long.
    if (strlen($rule['name']) > 32) {
      Migration::displayMessage(t('Failed to import field validation rule. The name must be no greater than 32 characters in length: @name',
        array('@name' => $rule['name'])
      ));
      return FALSE;
    }
    // Check the validator exists.
    ctools_include('plugins');
    $plugin = ctools_get_plugins('field_validation', 'validator', $rule['validator']);
    if (!$plugin) {
      Migration::displayMessage(t('Failed to import field validation rule, validator does not exist: @validator',
        array('@validator' => $rule['validator'])
      ));
      return FALSE;
    }
    // Check the name is unique across its entity_type/bundle.
    if (!empty($instance->entity_type) && !empty($instance->bundle)) {
      ctools_include('export');
      $rules = ctools_export_load_object('field_validation_rule', 'conditions', array('entity_type' => $instance->entity_type, 'bundle' => $instance->bundle));
      // If the name clashes, try to make it unique.
      if (isset($rules[$rule['name']])) {
        $rule['name'] = substr($rule['name'] . sha1($instance->field_name), 0, 32);
      }
      if (isset($rules[$rule['name']])) {
        Migration::displayMessage(t('Failed to import field validation rule: a rule with the name "@rule_name" already exists on "@entity_type" entities of bundle "@bundle".',
          array('@rule_name' => $rule['name'], '@entity_type' => $instance->entity_type, '@bundle' => $instance->bundle)
        ));
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Implementation of MigrateDestinationHandler::prepareRollback().
   */
  public function prepareRollback($instance_id) {
    if (!module_exists('field_validation')) {
      return;
    }
    if ($field_instance = field_read_instances(array('id' => $instance_id))) {
      $field_instance = reset($field_instance);
      db_delete('field_validation_rule')
        ->condition('field_name', $field_instance['field_name'])
        ->condition('entity_type', $field_instance['entity_type'])
        ->condition('bundle', $field_instance['bundle'])
        ->execute();
    }
  }
}
