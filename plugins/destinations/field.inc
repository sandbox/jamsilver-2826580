<?php

/**
 * @file
 * Support for field destinations.
 */

/**
 * Destination class implementing migration into {field_config}.
 */
class MigrateDestinationField extends MigrateDestination {
  static public function getKeySchema() {
    return array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'The primary identifier for a field.',
      ),
    );
  }

  public function __construct() {
    parent::__construct();
  }

  public function __toString() {
    $output = t('Field');
    return $output;
  }

  /**
   * Returns a list of fields available to be mapped for fields.
   *
   * @param Migration $migration
   *  Optionally, the migration containing this destination.
   * @return array
   *  Keys: machine names of the fields (to be passed to addFieldMapping)
   *  Values: Human-friendly descriptions of the fields.
   */
  public function fields($migration = NULL) {
    $fields = array(
      'field_name' => t("The name of the field. Each field name is unique within Field API. Non-deleted field names are unique. Maximum length is 32 characters."),
      'type' => t("The type of the field, such as 'text' or 'image'. Field types are defined by modules that implement hook_field_info()."),
      'entity types' => t('The array of entity types that can hold instances of this field. If empty or not specified, the field can have instances in any entity type.'),
      'cardinality' => t('The number of values the field can hold. Legal values are any positive integer or FIELD_CARDINALITY_UNLIMITED.'),
      'translatable' => t('Whether the field is translatable.'),
      'locked' => t("Whether or not the field is available for editing. If TRUE, users can't change field settings or create new instances of the field in the UI."),
      'storage_type' => t('The storage backend. The site default one is used if omitted.'),
      'storage_settings' => t('The storage settings. Each omitted setting is given the default value specified in hook_field_storage_info().'),
      'settings' => t("A sub-array of key/value pairs of field-type-specific settings. Each field type module defines and documents its own field settings. Each omitted setting is given the default value defined in its module's hook_field_info()."),
    );
    $fields += migrate_handler_invoke_all('field', 'fields', $migration);
    return $fields;
  }

  /**
   * Import a single row.
   *
   * @param stdClass $field
   *  Field object to build. Prefilled with any fields mapped in the Migration.
   * @param $row
   *  Raw source data object - passed through to prepare/complete handlers.
   *
   * @return array|bool
   *  Array of key fields of the object that was saved if
   *  successful. FALSE on failure.
   */
  public function import(stdClass $field, stdClass $row) {
    // Invoke migration prepare handlers
    $this->prepare($field, $row);

    // Fields are handled as arrays, so clone the object to an array.
    $field = clone $field;
    $field = (array) $field;

    if (!empty($field['storage_type'])) {
      $field['storage']['type'] = $field['storage_type'];
    }
    if (!empty($field['storage_settings'])) {
      $field['storage']['settings'] = $field['storage_settings'];
    }

    $existing_field = FALSE;
    if (!empty($field['id'])) {
      drupal_static_reset('_field_info_field_cache');
      if ($existing_field = field_info_field_by_id(array('id' => $field['id']))) {
        $field += $existing_field;
      }
    }

    $return = FALSE;
    if ($existing_field) {
      migrate_instrument_start('field_update_field');
      try {
        field_update_field($field);
        $this->numUpdated++;
        $return = array($field['id']);
      } catch (FieldException $e) {
        Migration::displayMessage(t('Failed to update existing field: !message',
          array('!message' => $e->getMessage())
        ));
      }
      migrate_instrument_stop('field_update_field');
    }
    else {
      migrate_instrument_start('field_create_field');
      try {
        $field = field_create_field($field);
        $this->numCreated++;
        $return = array($field['id']);
      } catch (FieldException $e) {
        Migration::displayMessage(t('Failed to create field: !message',
          array('!message' => $e->getMessage())
        ));
      }
      migrate_instrument_stop('field_create_field');
    }

    // Invoke migration complete handlers.
    if (isset($field['id'])) {
      $field = field_info_field_by_id($field['id']);
    }
    $field = (object) $field;
    $this->complete($field, $row);

    return $return;
  }

  public function prepare($field, stdClass $row) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    $field->migrate = array(
      'machineName' => $migration->getMachineName(),
    );

    // Call any general handlers.
    migrate_handler_invoke_all('field', 'prepare', $field, $row);
    // Then call any prepare handler for this specific Migration.
    if (method_exists($migration, 'prepare')) {
      $migration->prepare($field, $row);
    }
  }

  public function complete($field, stdClass $row) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    $field->migrate = array(
      'machineName' => $migration->getMachineName(),
    );
    // Call any general handlers.
    migrate_handler_invoke_all('field', 'complete', $field, $row);
    // Then call any complete handler for this specific Migration.
    if (method_exists($migration, 'complete')) {
      $migration->complete($field, $row);
    }
  }

  /**
   * Delete a single field.
   *
   * @param $fields
   *  Array of fields representing the key (in this case, just id).
   */
  public function rollback(array $fields) {
    $field_id = reset($fields);

    migrate_instrument_start('field_delete_field');
    $this->prepareRollback($field_id);
    if ($field = field_info_field_by_id($field_id)) {
      field_delete_field($field['field_name']);
    }
    $this->completeRollback($field_id);
    migrate_instrument_stop('field_delete_field');
  }

  /**
   * Give handlers a shot at cleaning up before a field has been rolled back.
   *
   * @param $field_id
   *  ID of the field about to be deleted.
   */
  public function prepareRollback($field_id) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    // Call any general handlers.
    migrate_handler_invoke_all('field', 'prepareRollback', $field_id);
    // Then call any complete handler for this specific Migration.
    if (method_exists($migration, 'prepareRollback')) {
      $migration->prepareRollback($field_id);
    }
  }

  /**
   * Give handlers a shot at cleaning up after a field has been rolled back.
   *
   * @param $field_id
   *  ID of the field which has been deleted.
   */
  public function completeRollback($field_id) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    // Call any general handlers.
    migrate_handler_invoke_all('field', 'completeRollback', $field_id);
    // Then call any complete handler for this specific Migration.
    if (method_exists($migration, 'completeRollback')) {
      $migration->completeRollback($field_id);
    }
  }
}
